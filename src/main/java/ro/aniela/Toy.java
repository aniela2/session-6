package ro.aniela;

public class Toy {
    private final int ageAllowed;
    private final String madeFrom;
    private final String name;
    // a Toy, at creation, is made from steel and is ment for children  younger than 3 years.
    // Also when this method is called it prints into the console “Code executed always”,
    // no matter if an exception occurs or not.

    public Toy(int ageAllowed, String madeFrom, String name) throws Exception6 {
        if (ageAllowed <= Constants.AGE_ALLOWED && madeFrom == Constants.MADE_FROM ){
        throw new Exception6();
    }

        this.ageAllowed =ageAllowed;
        this.madeFrom =madeFrom;
        this.name =name;

}

    public int getAgeAllowed() {
        return ageAllowed;
    }

    public String getName() {
        return name;
    }

    public String getMadeFrom() {
        return madeFrom;
    }

    public String toString() {
        return "Toy : ageAllowed " + ageAllowed + ", madeFrom" + madeFrom + ", name " + name;
    }

}
