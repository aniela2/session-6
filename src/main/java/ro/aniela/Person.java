package ro.aniela;

public class Person {
    private String name;
    private String surName;
    private int age;

    public Person(String name, String surname, int age) {
        if ((name == null || name.isBlank()) || (surname==null || surname.isBlank())) {
            throw new IllegalArgumentException("Is not ok");
        }

        this.name = name;
        this.surName = surname;
        this.age = age;


    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public int getAge() {
        return age;
    }
}
