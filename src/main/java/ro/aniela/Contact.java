package ro.aniela;

public record Contact(String name, String phoneNumber) {
}
