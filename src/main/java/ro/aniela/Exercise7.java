package ro.aniela;

public class Exercise7 {

    //Create a class Toy that has the following attributes:
    // ageAllowed, madeFrom, name. Create a method that throws the custom exception from before,
    // if a Toy, at creation, is made from steel and is ment for children  younger than 3 years.
    //Also when this method is called it prints into the console “Code executed always”,
    // no matter if an exception occurs or not.

    public static void main(String[] args) {
        try {
            Toy panda = new Toy(4, "steel", "bear");
            System.out.println(panda);
        } catch (Exception6 e) {
            e.printStackTrace();
        } finally {
            System.out.println("Code executed always");
        }

    }


}
