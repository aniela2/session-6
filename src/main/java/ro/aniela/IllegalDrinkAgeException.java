package ro.aniela;

public class IllegalDrinkAgeException extends RuntimeException{

    public static final int EUROPE_LEGAL_AGE = 18;
    public IllegalDrinkAgeException(String message){
        super(message);
    }
}
