package ro.aniela;

import java.time.LocalDate;

public final class Planet {
    private final String name;
    private final double mass;

    private final LocalDate dateOfDiscovery;

    public Planet(String name, double mass, LocalDate dateOfDiscovery) {
        this.name = name;
        this.mass = mass;
        this.dateOfDiscovery = dateOfDiscovery;

    }

    public String getName() {
        return name;
    }
    public double getMass(){
        return mass;
    }
    public LocalDate getDateOfDiscovery(){
        return dateOfDiscovery;
    }

}
