package ro.aniela;

public class Exercise2 {
    public static void main(String[]args){
        System.out.println(sum("1","a"));
    }

    private static double sum(String one, String two){
        try {
            return Double.valueOf(one) + Double.valueOf(two);
        }catch(NumberFormatException e){
            e.printStackTrace();
            return 0;
        }
    }
}
