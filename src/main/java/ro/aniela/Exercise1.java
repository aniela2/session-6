package ro.aniela;

public class Exercise1 {
    public static void main(String[] args) {
        System.out.println(division(1,0));

    }

    private static double division(double num1, double num2) {
        if (num2 == 0d) {
            throw new IllegalArgumentException("i caught an exception");
        }
        return num1 / num2;
    }
}