package ro.aniela;

public class Constants {
    private Constants() {
        throw new AssertionError();
    }

    public static final String MADE_FROM = "steel";
    public static final int AGE_ALLOWED = 3;
}
