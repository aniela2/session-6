package ro.aniela;

public class Exercise5 {
    public static void main(String[]args){
        Person j=new Person("name","surname", 18);
        System.out.println(isAllowedToDrink(j, IllegalDrinkAgeException.EUROPE_LEGAL_AGE));

    }
    private static boolean isAllowedToDrink(Person p, int legalAge){
        if(p.getAge()<legalAge){
            throw new IllegalDrinkAgeException("Is not allowed!");
        }
        return true;
    }
}
