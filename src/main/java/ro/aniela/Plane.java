package ro.aniela;

public final class Plane {
    private final double maxSpeed;
    private final int numberOfPassenger;
    private final int crewSize;

    public Plane(double maxSpeed, int numberOfPassenger, int crewSize) {
        this.maxSpeed=maxSpeed;
        this.numberOfPassenger=numberOfPassenger;
        this.crewSize=crewSize;

    }

    public double getMaxSpeed(){
        return maxSpeed;
    }
    public int getNumberOfPassenger(){
        return numberOfPassenger;
    }
    public int getCrewSize(){
        return crewSize;
    }
    public String toString(){
        return "Plane: maxSpeed " + maxSpeed + "nunberOfPasenger, " +numberOfPassenger + "crewSize" + crewSize;
    }

}
