package ro.aniela;

import java.time.LocalDate;

public class Exercise9 {
   public static void main(String[]args){
       Planet p= new Planet("n",12.4,LocalDate.of(1987,9,22));
       Planet j=new Planet("k",123.0,LocalDate.of(2021,2,22));
       System.out.println(p.getName() + " "+p.getDateOfDiscovery());
       System.out.println(j.getName() + " "+ j.getDateOfDiscovery());
   }
}
