package ro.aniela;

import java.util.Objects;

public record Shoes(Double size, String gender, String brand) {

    public Shoes(Double size, String gender, String brand){
        if(Objects.isNull(gender) || gender.isBlank()){
            this.gender= "unisex";
        }else{
            this.gender=gender;
        }
        this.brand= Objects.requireNonNull(brand);
        this.size=Objects.requireNonNull(size);
    }
}
